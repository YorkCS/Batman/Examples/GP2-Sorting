#!/bin/bash

docker run -v ${PWD}:/data --rm -e="GP2_FLAGS=-f -m -q" registry.gitlab.com/yorkcs/batman/gp2i programs/$1 graphs/$2
