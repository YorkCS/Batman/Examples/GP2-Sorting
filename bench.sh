#!/bin/bash

trap ctrl_c INT

function ctrl_c() {
    sleep 1
    printf "\n\n"
    docker-compose down -t 0
    exit 1
}

docker-compose pull
docker-compose up -d -t 0 --force-recreate
printf "\n"
docker pull registry.gitlab.com/yorkcs/batman/gp2-bench-cli:latest
printf "\n"
docker run -t -v ${PWD}:/data \
-e BENCH_HOST='docker.for.mac.localhost' \
-e GEN_MODE='list' \
-e GEN_STEP=$1 \
-e GEN_MIN=$1 \
-e GEN_MAX=$2 \
-e BENCH_RUNS=$3 \
--rm \
registry.gitlab.com/yorkcs/batman/gp2-bench-cli:latest \
programs/sort.gp2 programs/isort.gp2 \
|| true

printf "\n"
docker-compose logs
printf "\n"
docker-compose down -t 0
