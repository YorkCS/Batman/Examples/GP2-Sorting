# GP2 Sorting

Here lies an example of using the GP2 Benchmarking tooling, comparing the performance of a totally non-determinsitic sorting algorithm, with insertion sort.

## Prereqs

Any modern Linux or Mac running Docker 2018 CE or greater will suffice.

## Running

The following will run the insertion sort program on the randomly ordered list of 10 elements:

```bash
$ ./run.sh isort.gp2 random
```

You should see the following output:

```
[ (9, 9) (8, 8) (7, 7) (6, 6) (5, 5) 
  (4, 4) (3, 3) (2, 2) (1, 1) (0, 0) |
  (8, 8, 9, empty) (7, 7, 8, empty) (6, 6, 7, empty) 
  (5, 5, 6, empty) (4, 4, 5, empty) (3, 3, 4, empty) 
  (2, 2, 3, empty) (1, 1, 2, empty) (0, 0, 1, empty) ]
```

## Benchmarking

The following will run both programs 2 times on 10 randomly generated graphs of size between 100 and 1000:

```bash
$ ./bench.sh 100 1000 2
```

You should see something close to the following:

```
Pulling gp2i  ... done
Pulling gen   ... done
Pulling bench ... done
Creating network "gp2-sorting_default" with the default driver
Creating gp2-sorting_gp2i_1 ... done
Creating gp2-sorting_gen_1  ... done
Creating gp2-sorting_bench_1 ... done

latest: Pulling from yorkcs/batman/gp2-bench-cli
Digest: sha256:4169831839d50c08109db80747c3cd724f0072bd1f0c6d1ffc5317473b5a86fd
Status: Image is up to date for registry.gitlab.com/yorkcs/batman/gp2-bench-cli:latest
registry.gitlab.com/yorkcs/batman/gp2-bench-cli:latest

100%|█████████████████████████████████████| 10/10 [00:00<00:00, 246.59 graphs/s]
100%|████████████████████████████████████████| 40/40 [00:19<00:00,  2.00 runs/s]

Results for programs/sort.gp2:

|   Graph |   Nodes |   Edges | Success   | Time      |
|---------|---------|---------|-----------|-----------|
|       1 |     100 |      99 | 100.00%   | 15.61ms   |
|       2 |     200 |     199 | 100.00%   | 33.87ms   |
|       3 |     300 |     299 | 100.00%   | 91.79ms   |
|       4 |     400 |     399 | 100.00%   | 174.46ms  |
|       5 |     500 |     499 | 100.00%   | 340.28ms  |
|       6 |     600 |     599 | 100.00%   | 581.13ms  |
|       7 |     700 |     699 | 100.00%   | 954.90ms  |
|       8 |     800 |     799 | 100.00%   | 1486.35ms |
|       9 |     900 |     899 | 100.00%   | 2052.96ms |
|      10 |    1000 |     999 | 100.00%   | 2767.81ms |

Results for programs/isort.gp2:

|   Graph |   Nodes |   Edges | Success   | Time    |
|---------|---------|---------|-----------|---------|
|       1 |     100 |      99 | 100.00%   | 14.89ms |
|       2 |     200 |     199 | 100.00%   | 15.18ms |
|       3 |     300 |     299 | 100.00%   | 18.03ms |
|       4 |     400 |     399 | 100.00%   | 21.76ms |
|       5 |     500 |     499 | 100.00%   | 28.98ms |
|       6 |     600 |     599 | 100.00%   | 33.54ms |
|       7 |     700 |     699 | 100.00%   | 43.31ms |
|       8 |     800 |     799 | 100.00%   | 54.30ms |
|       9 |     900 |     899 | 100.00%   | 63.16ms |
|      10 |    1000 |     999 | 100.00%   | 71.24ms |

Attaching to gp2-sorting_bench_1, gp2-sorting_gen_1, gp2-sorting_gp2i_1

Stopping gp2-sorting_bench_1 ... done
Stopping gp2-sorting_gen_1   ... done
Stopping gp2-sorting_gp2i_1  ... done
Removing gp2-sorting_bench_1 ... done
Removing gp2-sorting_gen_1   ... done
Removing gp2-sorting_gp2i_1  ... done
Removing network gp2-sorting_default
```